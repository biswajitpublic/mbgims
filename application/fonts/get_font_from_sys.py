import matplotlib.font_manager
from PIL import ImageFont

# Iterate over all font files known to matplotlib
def get_font_list()-> dict:
    font_list = {}
    sys_fonts = matplotlib.font_manager.findSystemFonts()
    for filename in sys_fonts: 
        # Avoid these two trouble makers - don't know why they are problematic
        if "Emoji" not in filename and "18030" not in filename:
            # Look up what PIL knows about the font
            font = ImageFont.FreeTypeFont(filename)
            name, weight = font.getname()
            if name in font_list:
                font_list[name][weight] = filename
            else:
                font_list[name] = {weight:filename}
            # print(f'File: {filename}, fontname: {name}, weight: {weight}')
    return font_list