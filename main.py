from PySide6.QtWidgets import QApplication
from PySide6.QtGui import QFont
from gui.mainapp import MainWindow


app = QApplication()
app.setFont(QFont("verdana",11))
window = MainWindow()
window.show()

app.exec()