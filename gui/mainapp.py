from PySide6.QtWidgets import QWidget, QMainWindow, QHBoxLayout, QVBoxLayout
from PySide6.QtCore import Qt
from gui.toolbar.maintoolbar import MainToolBar
from gui.toolbar.secondary_toolbar import SecondaryToolBar
from gui.menubar.mainmenubar import MainMenuBar
from gui.tabbar.landingpage import LandingPage

class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("MBGIMS")
        self.main_screen = MainScreen()
        self.setCentralWidget(self.main_screen)
        self.main_toolbar = MainToolBar()
        self.addToolBar(Qt.LeftToolBarArea, self.main_toolbar)
        self.secondary_toolbar = SecondaryToolBar()
        self.addToolBar(Qt.ToolBarArea.TopToolBarArea, self.secondary_toolbar)
        self.menu_bar = MainMenuBar(self)
        self.setMenuBar(self.menu_bar)
        self.showMaximized()


class MainScreen(QWidget):
    def __init__(self):
        super().__init__()
        layout = QVBoxLayout()
        self.landing_page = LandingPage()
        layout.addWidget(self.landing_page)
        layout.setAlignment(Qt.AlignmentFlag.AlignTop)
        self.setLayout(layout)

        