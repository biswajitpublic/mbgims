from PySide6.QtWidgets import QTabWidget, QWidget, QVBoxLayout

class LandingPage(QTabWidget):
    def __init__(self):
        super().__init__()
        self.setTabsClosable(True)
        self.addTab(FirstTab(), "Open Image")
        self.tabCloseRequested.connect(self.remove_tab)

    def remove_tab(self, index):
        # Remove the tab at the given index
        widget = self.widget(index)
        self.removeTab(index)
        widget.deleteLater()

class FirstTab(QWidget):
    def __init__(self):
        super().__init__()
        main_layout_v = QVBoxLayout()
        self.setLayout(main_layout_v)