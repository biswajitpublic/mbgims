from PySide6.QtWidgets import QMenuBar, QMenu, QFileDialog
from PySide6.QtGui import QAction
from gui.image_processing.create_new_image import CreateNewImageWindow, NewImageTab
from PIL import Image
import os

class MainMenuBar(QMenuBar):
    def __init__(self, parent):
        super().__init__()
        self.addMenu(FileMenu(parent))

class FileMenu(QMenu):
    def __init__(self, parent):
        self.parent = parent
        super().__init__()
        self.setTitle("&File")
        open_img = QAction("Open Image", self)
        open_img.triggered.connect(self.open_image)
        self.addAction(open_img)
        new_img = QAction("New Image", self)
        new_img.triggered.connect(self.new_image)
        self.addAction(new_img)

    def open_image(self):
        file_path, _ = QFileDialog.getOpenFileName(self, "Open Image", "", "Image Files (*.png *.jpg *.bmp)")
        img = Image.open(file_path)
        name = os.path.basename(img.filename)
        img = img.convert("RGBA")
        new_tab = NewImageTab(parent=self.parent,image=img)
        self.parent.main_screen.landing_page.addTab(new_tab, name)
        self.parent.main_screen.landing_page.setCurrentIndex(self.parent.main_screen.landing_page.count()-1)


    def new_image(self):
        self.create_image_window = CreateNewImageWindow(self.parent)
        self.create_image_window.show()
        self.create_image_window.closed.connect(self.create_image_window.deleteLater)
        