from PySide6.QtWidgets import QToolBar, QPushButton
from PySide6.QtCore import Qt, QSize
from PySide6.QtGui import QIcon


class MainToolBar(QToolBar):
    def __init__(self):
        super().__init__()
        self.setContextMenuPolicy(Qt.ContextMenuPolicy.PreventContextMenu)
        self.setFixedWidth(40)
        self.addWidget(CropTool())
        self.text_tool = TextTool()
        self.addWidget(self.text_tool)
        self.zoom_in = ZoomIn()
        self.addWidget(self.zoom_in)
        self.zoom_out = ZoomOut()
        self.addWidget(self.zoom_out)

class CropTool(QPushButton):
    def __init__(self):
        super().__init__()
        self.setIcon(QIcon('application/components/icons/crop.svg'))
        self.setIconSize(QSize(36,36))
        self.setCheckable(True)
        self.setStyleSheet(r'QPushButton {border: none} QPushButton:checked {background-color: #C5C5C5}')

class TextTool(QPushButton):
    def __init__(self):
        super().__init__()
        self.setIcon(QIcon('application/components/icons/text.svg'))
        self.setIconSize(QSize(36,36))
        self.setCheckable(True)
        self.setStyleSheet(r'QPushButton {border: none} QPushButton:checked {background-color: #C5C5C5}')

class ZoomIn(QPushButton):
    def __init__(self):
        super().__init__()
        self.setIcon(QIcon('application/components/icons/zoom_in.svg'))
        self.setIconSize(QSize(36,36))
        self.setStyleSheet(r'QPushButton {border: none} QPushButton:pressed {background-color: #C5C5C5}')

class ZoomOut(QPushButton):
    def __init__(self):
        super().__init__()
        self.setIcon(QIcon('application/components/icons/zoom_out.svg'))
        self.setIconSize(QSize(36,36))
        self.setStyleSheet(r'QPushButton {border: none} QPushButton:pressed {background-color: #C5C5C5}')