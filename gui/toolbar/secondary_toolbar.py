from PySide6.QtWidgets import QToolBar, QWidget, QHBoxLayout, QLabel
from PySide6.QtCore import Qt

class SecondaryToolBar(QToolBar):
    def __init__(self):
        super().__init__()
        self.setContextMenuPolicy(Qt.ContextMenuPolicy.PreventContextMenu)
        # self.setFixedHeight(35)
        self.widget = QWidget()
        self.layout = QHBoxLayout()
        self.widget.setLayout(self.layout)
        self.addWidget(self.widget)
        