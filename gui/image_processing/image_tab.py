from PySide6.QtWidgets import QLabel, QGraphicsView, QGraphicsScene, QGraphicsProxyWidget, QSizePolicy
from PySide6.QtCore import Qt
from PySide6.QtGui import QPixmap, QTransform, QImage
from PIL import Image, ImageQt
from gui.image_processing.toggle_text_edit import toggle_text_edit
from gui.image_processing.text_edit import WriteTextOnImage

class NewImageTab(QGraphicsView):
    def __init__(self,parent, width: int = None, height: int = None, color: str = None, image: Image = None, ):
        self.parent = parent
        super().__init__()
        self.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        if image is None:
            if color == 'Black':
                color = (0,0,0)
            elif color == 'White':
                color = (255,255,255)
            elif color == 'Green':
                color = (0,255,0)
            elif color == 'Red':
                color = (255,0,0)
            elif color == 'Blue':
                color = (0,0,255)
            else:
                color = (255,255,255,0)
            self.img = Image.new(mode="RGBA",size=(width, height), color=color)
        else:
            self.img = image
        q_image = ImageQt.ImageQt(self.img)
        self.image = QImage(q_image)
        self.scale_factor = 1
        self.show_image = QLabel()
        self.show_image.setStyleSheet('border: 3px solid black; border-style: dotted')
        self.show_image.setPixmap(QPixmap.fromImage(self.image))
        self.show_image.setMinimumSize(self.img.width, self.img.height)

        # create a proxy widget for the label
        proxy = QGraphicsProxyWidget()
        proxy.setWidget(self.show_image)

        self.setScene(QGraphicsScene(self))
        self.scene().addItem(proxy)
        

        self.setAlignment(Qt.AlignmentFlag.AlignCenter)

        self.items = []
        self.current_item = None

        self.parent.main_toolbar.zoom_in.clicked.connect(self.zoom_in_clicked)
        self.parent.main_toolbar.zoom_out.clicked.connect(self.zoom_out_clicked)
        self.parent.main_toolbar.text_tool.clicked.connect(lambda: toggle_text_edit(self, self.parent))

    def mousePressEvent(self, event):
        checked = self.parent.main_toolbar.text_tool.isChecked()
        if checked == True:
            if event.button() == Qt.MouseButton.LeftButton:
                WriteTextOnImage(self, event)
                

    def zoom_in_clicked(self):
        # Increase the zoom level
        self.scale_factor *= 1.1
        transform = QTransform()
        transform.scale(self.scale_factor, self.scale_factor)
        self.setTransform(transform)

    def zoom_out_clicked(self):
        # Decrease the zoom level
        self.scale_factor /= 1.1
        transform = QTransform()
        transform.scale(self.scale_factor, self.scale_factor)
        self.setTransform(transform)


    def main_font_changed(self, font):
        sub_fonts = [sub_font for sub_font in self.all_font_family[font]]
        self.sub_font_combo_box.clear()
        self.sub_font_combo_box.addItems(sub_fonts)
        
    
    def save_image_clicked(self):
        pass