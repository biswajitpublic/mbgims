from PySide6.QtWidgets import QWidget, QVBoxLayout, QHBoxLayout, QLabel, QLineEdit, QApplication, QComboBox, QPushButton
from PySide6.QtCore import Qt, Signal
from gui.image_processing.image_tab import NewImageTab

class CreateNewImageWindow(QWidget):
    closed = Signal()
    def __init__(self, parent):
        self.parent = parent
        super().__init__()
        self.setWindowModality(Qt.WindowModality.ApplicationModal)
        self.setGeometry(0,0,400,300)
        self.setFixedSize(400,300)
        screen_geometry = QApplication.primaryScreen().geometry()
        x = (screen_geometry.width() - self.width()) / 2
        y = (screen_geometry.height() - self.height()) / 2
        self.move(x, y)
        # Disable minimize and maximize buttons
        self.setWindowFlags(Qt.WindowType.CustomizeWindowHint | Qt.WindowType.WindowCloseButtonHint)
        main_layout_v = QVBoxLayout()
        main_layout_v.setAlignment(Qt.AlignmentFlag.AlignTop)
        self.setLayout(main_layout_v)

        ask_width_box = QHBoxLayout()
        width_label = QLabel("Image Width(px):")
        width_label.setFixedWidth(150)
        self.width = QLineEdit()
        ask_width_box.addWidget(width_label)
        ask_width_box.addWidget(self.width)

        ask_height_box = QHBoxLayout()
        height_label = QLabel("Image Height(px):")
        height_label.setFixedWidth(150)
        self.height = QLineEdit()
        ask_height_box.addWidget(height_label)
        ask_height_box.addWidget(self.height)

        background_box = QHBoxLayout()
        bg_label = QLabel("Background:")
        bg_label.setFixedWidth(150)
        self.background = QComboBox()
        self.background.addItems(['Black', 'White', 'Green', 'Red', 'Blue', 'Alpha'])
        background_box.addWidget(bg_label)
        background_box.addWidget(self.background)

        btn_box = QHBoxLayout()
        btn_box.setAlignment(Qt.AlignmentFlag.AlignCenter)
        self.create_btn = QPushButton("Create")
        self.create_btn.clicked.connect(self.create_btn_clicked)
        btn_box.addWidget(self.create_btn)


        main_layout_v.addLayout(ask_width_box)
        main_layout_v.addLayout(ask_height_box)
        main_layout_v.addLayout(background_box)
        main_layout_v.addLayout(btn_box)

    def create_btn_clicked(self):
        width = int(self.width.text())
        height = int(self.height.text())
        color = self.background.currentText()
        new_tab = NewImageTab(self.parent, width, height, color)
        self.parent.main_screen.landing_page.addTab(new_tab, 'untitled')
        self.parent.main_screen.landing_page.setCurrentIndex(self.parent.main_screen.landing_page.count()-1)
        self.close()

    def closeEvent(self, event):
        self.closed.emit()


    

