from PySide6.QtWidgets import QInputDialog
from PySide6.QtGui import QImage, QPixmap
from PIL import ImageFont, ImageQt, ImageDraw

def WriteTextOnImage(self, event):
    text, ok = QInputDialog.getText(self, 'Enter Text', 'Enter the text:')
    if ok:
        cursor_pos = self.mapToScene(event.pos())
        self.scene().addItem(self.current_item)
        self.items.append(self.current_item)
        x, y = cursor_pos.x(), cursor_pos.y()
        font_family = self.font_combo_box.currentText()
        sub_font = self.sub_font_combo_box.currentText()
        font_size = int(self.font_size.text())
        font_path = self.all_font_family[font_family][sub_font]
        
        font = ImageFont.truetype(font_path, font_size)
        draw = ImageDraw.Draw(self.img)
        draw.text((x,y), text, (0,0,0), font=font)
        q_image = ImageQt.ImageQt(self.img)
        self.image = QImage(q_image)
        self.show_image.setPixmap(QPixmap.fromImage(self.image))
        self.show_image.setMinimumSize(self.img.width, self.img.height)