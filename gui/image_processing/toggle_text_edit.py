from PySide6.QtWidgets import QComboBox, QLabel, QLineEdit
from application.fonts.get_font_from_sys import get_font_list

def toggle_text_edit(self,_parent):
    parent = _parent
    checked = parent.main_toolbar.text_tool.isChecked()
    if checked == True:
        for i in reversed(range(parent.secondary_toolbar.layout.count())):
            parent.secondary_toolbar.layout.itemAt(i).widget().deleteLater()
        font_label = QLabel("Font:")
        self.font_combo_box = QComboBox()
        
        # Get a list of all available fonts in the system
        self.all_font_family = get_font_list()
        main_fonts = sorted([font for font in self.all_font_family])
        # Add each font to the QComboBox
        self.font_combo_box.addItems(main_fonts)
        parent.secondary_toolbar.layout.addWidget(font_label)
        parent.secondary_toolbar.layout.addWidget(self.font_combo_box)
        sub_font_label = QLabel("Sub Font:")
        self.sub_font_combo_box = QComboBox()
        sub_fonts = [sub_font for sub_font in self.all_font_family[main_fonts[0]]]
        self.sub_font_combo_box.addItems(sub_fonts)
        parent.secondary_toolbar.layout.addWidget(sub_font_label)
        parent.secondary_toolbar.layout.addWidget(self.sub_font_combo_box)
        self.font_combo_box.currentTextChanged.connect(self.main_font_changed)
        font_size_label = QLabel("Font Size:")
        self.font_size = QLineEdit()
        # self.setFixedWidth(30)
        self.font_size.setText("12")
        parent.secondary_toolbar.layout.addWidget(font_size_label)
        parent.secondary_toolbar.layout.addWidget(self.font_size)