from PySide6.QtCore import Qt
from PySide6.QtGui import QFont, QPainter

from PySide6.QtWidgets import QGraphicsTextItem, QGraphicsItem


class TextItem(QGraphicsTextItem):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.setFont(QFont("Arial", 16))
        self.setDefaultTextColor(Qt.black)
        self.setTextInteractionFlags(Qt.TextEditable | Qt.TextEditorInteraction)
        self.setFlag(QGraphicsItem.ItemIsMovable)

    def focusOutEvent(self, event):
        self.setTextInteractionFlags(Qt.TextEditorInteraction)
        super().focusOutEvent(event)

    def mouseDoubleClickEvent(self, event):
        self.setTextInteractionFlags(Qt.TextEditorInteraction | Qt.TextEditable)
        super().mouseDoubleClickEvent(event)

    def boundingRect(self):
        return super().boundingRect().adjusted(-2, -2, 2, 2)

    def shape(self):
        path = super().shape()
        path.addRect(self.boundingRect())
        return path

    def setText(self, text: str):
        super().setText(text)
        self.document().setTextWidth(self.boundingRect().width())

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Enter or event.key() == Qt.Key_Return:
            self.clearFocus()
        else:
            super().keyPressEvent(event)

    def paint(self, painter, option, widget):
        painter.setRenderHint(QPainter.Antialiasing)
        super().paint(painter, option, widget)
